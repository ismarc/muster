package com.muster

import com.muster.tokens.Token

class Context(ids: Map[String, Token], inputs: List[Token], parent: Option[Context]) {
  lazy val child = new Context(Map[String, Token](), List[Token](), Some(this))

  def +(name: String, binding: Token) = {
    val newIDs = ids + (name -> binding)
    new Context(newIDs, inputs, parent)
  }

  def addInput(binding: Token) = {
    val newInputs = binding +: inputs
    new Context(ids, newInputs, parent)
  }

  def resolve(id: String): Option[Token] = {
    if (ids.contains(id)) {
      Some(ids(id))
    } else {
      parent match {
        case Some(c) => c.resolve(id)
        case None => None
      }
    }
  }

  def reverseResolve(identifiers: List[String]): Option[Token] = {
    reverseResolve(identifiers, None)
  }

  def reverseResolve(identifiers: List[String], token: Option[Token]): Option[Token] = {
    if (identifiers.length == 1) {
      resolve(identifiers.head) match {
        case Some(i_token) => {
          token match {
            case Some(n_token) => {
              token
            }
            case None => {
              Some(i_token)
            }
          }
        }
        case None => {
          None
        }
      }
    } else {
      resolve(identifiers.head) match {
        case Some(i_token) => {
          parent match {
            case Some(context) => {
              token match {
                case Some(n_token) => {
                  context.reverseResolve(identifiers.tail, Some(n_token))
                }
                case None => {
                  context.reverseResolve(identifiers.tail, Some(i_token))
                }
              }
            }
            case None => {
              None
            }
          }
        }
        case None => {
          None
        }
      }
    }
  }

  def top(): Context = {
    parent match {
      case Some(context) => { context.top() }
      case None => this
    }
  }

  def resolveInputs(): List[Token] = {
    inputs.reverse
  }

  def allIds(): Map[String, Token] = {
    ids
  }
}

object EmptyContext extends Context(Map[String, Token](), List[Token](), None)

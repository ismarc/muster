package com.muster

import com.muster.tokens._

class Interpreter (tree: Token) {
  /* Interprets the supplied AST. */
  def run(args: List[String]) {
    val symbolTable: Map[String, Symbol] = buildSymbolTable(tree)
    symbolTable.get("main") match {
      case Some(main) => {
        /* Add in the arguments as input to the interpreter */
        val context = new Context(Map[String, Token](), List[Token](), None)
        walkTree(main.tree, symbolTable, args.foldLeft(context)((n_context, arg) => {
          n_context.addInput(LiteralToken(arg))
        }))
      }
      case None => {
        println("No main symbol defined")
        sys.exit(100)
      }
    }
  }

  /* Evaluates an AST using a given symbol table and context for evaluation */
  def walkTree(tree: Token, symbolTable: Map[String, Symbol], context: Context): Option[Token] = {
    tree match {
      /* ProgramToken contains a series of operations.  The result of the walk is the last
       * evaulated operation in the series.
       */
      case ProgramToken(operations) => {
        operations.foldLeft(None: Option[Token])((option, operation) => {
          walkTree(operation, symbolTable, context)
        })
      }
      /* A ProgramOperationToken holds a single operation that needs evaluating. */
      case ProgramOperationToken(operation) => {
        walkTree(operation, symbolTable, context)
      }
      /* An operation token has an expression on the left and a optional operation to the right.  An operation
       * is call semantics, so invoke a call (that may invoke a walkTree) feeding the walk results of the
       * right into the left.  The result is a right-based evaluation order of tokens for feeding input.  If
       * there is no right, it indicates that left needs independent evaluation.
       */
      case OperationToken(left, right) => {
        right match {
          case Some(operation) => {
            call(left, walkTree(operation, symbolTable, context), symbolTable, context)
          }
          case None => {
            walkTree(left, symbolTable, context)
          }
        }
      }
      /* The following are all Expression types, indicating they can (but aren't required to)
       * result in a singular result.
       */
      case IdentifierToken(identifier, ident_type) => {
        /* Walk the tree of the ident_type to retrieve a matching pattern for type information */
        val type_info = ident_type match {
          case Some(symbol_type) => {
            walkTree(symbol_type, symbolTable, context)
          }
          case None => {
            None
          }
        }
        /* Walk the tree to resolve information about the identifier */
        val identifier_info = walkTree(identifier, symbolTable, context)

        /* Validate the information and return the value */
        val rawIdentifier = type_info match {
          case Some(type_info) => {
            identifier_info match {
              case Some(rawToken: RawIdentifierToken) => {
                /* TODO: This should do a symbol lookup and return the value/AST associated with the
                 * RawIdentifierToken.
                 */
                if (isType(rawToken, type_info)) {
                  resolveRawIdentifier(rawToken, symbolTable, context)
                } else {
                  /* This should throw an exception rather than just returning a None value */
                  None
                }
              }
              case None => {
                None
              }
            }
          }
          case None => {
            identifier_info match {
              case Some(rawToken: RawIdentifierToken) => {
                resolveRawIdentifier(rawToken, symbolTable, context)
              }
              case None => {
                identifier_info
              }
            }
          }
        }

        rawIdentifier match {
          case Some(token) => {
            walkTree(token, symbolTable, context)
          }
          case None => {
            /* This should throw an exception rather than just returning a None value */
            None
          }
        }
      }

      /* A RawIdentifierToken has a list of qualifiers to be resolved. */
      case RawIdentifierToken(identifiers) => {
        /* identifiers is a List[String] of items enumberating a name that can be resolved in the
         * current context or a name that can be resolved in the symbol table.  Symbol lookup/resolution happens
         * at the IdentifierToken level, so this needs to just return the RawIdentifierToken
         */
        Some(tree)
      }
      /* Indicates that the contained operation should be called serially. */
      case SerialToken(operation) => {
        /* TODO: Make this actually call it serially rather than just the normal operation interpretation. */
        walkTree(operation, symbolTable, context)
      }
      /* A ConditionToken holds a BlockToken.  The CompositeToken of the BlockToken should be evaluated and
       * if it evaluates to a non-falsey value, the block should be executed.
       * TODO: Make it actually check the condition.
       */
      case ConditionToken(condition) => {
        /* Extract the CompositeToken and Operations from the condition */
        condition match {
          case BlockToken(composite, ProgramToken(operations)) => {
            /* Here is where we should check if the composite is non-falsey */
            operations.foldLeft(None: Option[Token])((option, operation) => {
              walkTree(operation, symbolTable, context)
            })
          }
          case _ => {
            None
          }
        }
      }
      /* A DeclarationToken holds a BlockToken.  If it is being reached in this context, it is the evaluation
       * of a BlockToken and walkTree should just be called on the block itself.
       */
      case DeclarationToken(block) => {
        walkTree(block, symbolTable, context)
      }
      /* A DefinitionToken only adds a symbol to the current context but may contain its own definitions.
       * The symbolTable should already hold every definition available, while the context holds the values
       * of any inputs if the block is to be evaluated.  So there is nothing to do here but ignore it.
       */
      case DefinitionToken(identifier, declaration) => {
        None
      }
      /* A BlockToken indicates a given scope.  Input to the block is locally tied to the operations in
       * the composite while the program contains the list of operations to evaluate in the new context.
       * If a BlockToken is reached, that indicates it is not part of a DefinitionToken and should be
       * evaluated.
       * TODO: Actually implement this
       */
      case BlockToken(composite, program) => {
        /* Create a context for the contents of the BlockToken (it's ProgramToken).  It is a bare context as a
         * child of the existing context.  Each of the items defined in Context.inputs is zipped with the parameters
         * available in the composite that defines inputs.
         */
        val block_context = composite.operations match {
          case Some(operations) => {
            println("Operations section")
            println(operations)
            println(context.resolveInputs())
            operations.zip(context.resolveInputs()).foldLeft(new Context(Map[String, Token](), List[Token](),
                                                                         Some(context)))((new_context, member) => {
              println(member)
              new_context + (resolveOperationToIdentifierName(member._1), member._2)
            })
          }
          case None => {
            new Context(Map[String, Token](), List[Token](), Some(context))
          }
        }

        println("Working in block")
        for (key <- block_context.allIds().keys) {
          println(block_context.resolve(key))
        }
        walkTree(program, symbolTable, block_context)
      }
      /* A CompositeToken is a list of operations that indicate input to a block, a (potentially) 'singular'
       * value or a conditional statement to be evaluated.  If it's reached in a bare case like this (as in,
       * not handled at ConditionalToken or BlockToken level) it is assumed to be used as a 'singular' value.
       * TODO: Define the semantics of how to hand off the values.  It's handled inside the call function,
       * which means that technically we don't need to handle it here and instead need to return the token.
       */
      case CompositeToken(operations) => {
        Some(tree)
      }
      case token: LiteralToken => {
        Some(token)
      }
      case x => {
        println("Unknown token found.")
        println(x)
        None
      }
    }
  }

  /* Determines if the supplied RawIdentifierToken has the same capabilities as the supplied ident_type Token.
   * This is for the capabilities based type enforcement.
   * TODO: Implement actual checking.
   */
  def isType(identifier: RawIdentifierToken, ident_type: Token): Boolean = {
    true
  }

  /* Given a RawIdentifier, find the matching AST Token it is a label for.
   * Done in priority order of: Context, Symbol Table, Libraries.
   * TODO: Include library lookup.
   */
  def resolveRawIdentifier(identifier: RawIdentifierToken,
                           symbolTable: Map[String, Symbol], context: Context): Option[Token] = {
    println("Resolving Raw Identifier")
    println(identifier)
    /* A RawIdentifierToken is a list of strings, indicating a hierarchical relationship. */
    /* TODO: Fix Context so that we can validate namespacing, such as com -> muster -> util matches rather than
     * local block -> util or com -> <whatever>.  This is a structural change to Context to be more like
     * the Symbol Table (in fact, the code for Symbol Tables and Contexts should be merged).  For now, don't
     * check the Context
     */
    context.reverseResolve(identifier.identifiers) match {
      /* This would be a matched context */
      case Some(token) => { 
        println("Matched context")
        println(token)
        Some(token)
      }
      /* Identifier doesn't resolve in the Context */
      case None => {
        val table = new SymbolTable(symbolTable)
        val symbol = table.resolve(identifier.identifiers)

        symbol match {
          case Some(value) => {
            println("found tree:")
            println(value.tree)
            Some(value.tree)
          }
          case None => {
            println("Didn't find tree")
            None
          }
        }
      }
    }
  }

  /* Takes an OperationToken that is assumed to contain an IdentifierToken.  This resolves the token down to the
   * string representation of that name.
   */
  def resolveOperationToIdentifierName(operation: OperationToken): String = {
    operation.left match {
      case IdentifierToken(identifier, ident_type) => {
        identifier.identifiers(0)
      }
      /* OperationToken does not contain an IdentiferToken.  There is no recovery, this should throw an exception
       * or something
       */
      case _ => {
        ""
      }
    }
  }

  /* Processes call semantics for feeding input into an expression.
   * TODO: Implement this
   */
  def call(target: Token, input: Option[Token],
           symbolTable: Map[String, Symbol], context: Context): Option[Token] = {
    println("Call for:")
    println(target)
    input match {
      /* A CompositeToken indicates that the parameters are to be bound as input. */
      case Some(CompositeToken(token)) => {
        val new_context = token match {
          case Some(operations) => {
            new Context(Map[String, Token](), operations, Some(context))
          }
          case None => {
            new Context(Map[String, Token](), List[Token](), Some(context))
          }
        }
        walkTree(target, symbolTable, new_context)
      }
      case Some(token: LiteralToken) => {
        walkTree(target, symbolTable, new Context(Map[String, Token](), List[Token](token), Some(context)))
      }
      case None => {
        walkTree(target, symbolTable, context)
      }
    }
  }

  /* Builds the symbol table for use in interpretation.
   * The logic looks more complicated than it is.  For each definition, assign to it the AST it represents and
   * a symbol table of all definitions it contains (represented by a map of symbol -> (AST, symbol table) pairs.
   * This allows scope-level resolution of symbols (given util.map, inside the util definition, it can be
   * referred to as just map).
   * Returns the full AST symbol table.
   */
  def buildSymbolTable(tree: Token): Map[String, Symbol] = {
    /* Type of token determines how the symbol table is built. */
    tree match {
      /* Definitions indicate a new scope, add a new entry and recurse into buildSymbolTable with the AST of the
       * symbol.
       */
      case DefinitionToken(identifier, declaration) => {
        Map[String, Symbol](identifier.identifier.identifiers(0) -> Symbol(declaration,
                                                                           buildSymbolTable(declaration)))
      }
      /* A Program contains multiple operations.  Each operation needs to be processed and the result added
       * to the symbol table.
       */
      case ProgramToken(operations) => {
        operations.foldLeft(Map[String, Symbol]())((acc, operation) => {
          acc ++ buildSymbolTable(operation)
        })
      }
      /* A ProgramOperationToken does not add to the symbol table, so process its components */
      case ProgramOperationToken(operation) => {
        buildSymbolTable(operation)
      }
      /* An operation token has a left and an optional right AST.  Both need to be processed and any definitions
       * added to the symbol table.
       */
      case OperationToken(left, right) => {
        right match {
          case Some(token) => {
            buildSymbolTable(left) ++ buildSymbolTable(token)
          }
          case None => {
            buildSymbolTable(left)
          }
        }
      }
      /* Literals do not have an AST and do not have an entry in the symbol table */
      case LiteralToken(literal) => {
        Map[String, Symbol]()
      }
      /* Declarations do not have an entry in the symbol table, but may contain statements that do */
      case DeclarationToken(block) => {
        buildSymbolTable(block)
      }
      /* Blocks do not have an entry in the symbol table, but may contain statements that do */
      case BlockToken(composite, statements) => {
        buildSymbolTable(statements)
      }
      /* Composite tokens contain 0 or more operations, each of which may add to the symbol table.  Each one
       * must be processed and any appropriate symbols added to the symbol table.
       */
      case CompositeToken(operations) => {
        operations match {
          case Some(compositeOperations) => {
            compositeOperations.foldLeft(Map[String, Symbol]())((acc, operation) => {
              acc ++ buildSymbolTable(operation)
            })
          }
          case None => {
            Map[String, Symbol]()
          }
        }
      }
      /* Conditions do not add to the symbol table but may contain operations that do. */
      case ConditionToken(conditionBlock) => {
        buildSymbolTable(conditionBlock)
      }
      /* All other tokens cannot contain a symbol table definition, so return an empty list. */
      case _ => {
        Map[String, Symbol]()
      }
    }
  }
}

package com.muster

import scala.collection.JavaConversions._
import scala.collection.immutable.HashMap
import scala.io.Source
import scala.util.parsing.combinator.syntactical.StandardTokenParsers

import com.muster.tokens._

/** Main entry point for Muster Interpreter.
 *  Defines the grammar and parser for the Muster language, parses the input and the hands it off to the
 *  interpreter.
 */
object Muster extends StandardTokenParsers {
  /* declare: creates a block that takes 0 or more input to a series of operations
   * define: creates a label for a declaration
   * when: conditional check
   * serial: communicates to the interpreter/compiler that everything in the block should be done
   *         in sequence rather than in parallel.
   */
  lexical.reserved += ("declare", "define", "when", "serial")
  /* '{}': wraps operations in a block
   * '()': wraps composite values and input to declarations
   * ':=': used in assigning a label to a declaration (creating a definition)
   * '<<': calls the symbol on the left-hand side with the value on the right-hand side
   * ';': terminates an operation
   * ',': separates operations in a composite value
   * '.': separates symbols in an identifier
   * ':': separates identifiers from type specification
   * '/**/': block comments
   */
  lexical.delimiters += ("{","}","(",")", ":=", "<<", ";", ",", ".", ":", "/*", "*/")

  // Grammar
  /* Bare symbols, all resolvable to a specific definition in a certain context.
   * Examples:
   * a
   * a.b
   * a.c.d.e
   */
  lazy val RAW_IDENTIFIER = repsep(ident, ".") ^^ { case s => RawIdentifierToken(s) }
  /* RawIdentifier with included type information (encoded as a RawIdentifier)
   * Examples:
   * a
   * a: A
   * a: A.B
   */
  lazy val IDENTIFIER = RAW_IDENTIFIER ~ (":" ~> RAW_IDENTIFIER).? ^^ {
    case identifier ~ ident_type => IdentifierToken(identifier, ident_type)
  }
  /* Multi-Operation holding entity.  When evaluated, each is independently evaluated.
   * Examples:
   * ()
   * (a)
   * (a,b)
   * (a: A, b)
   * (a, b: B)
   */
  lazy val COMPOSITE = "(" ~> repsep(OPERATION, ",").? <~ ")" ^^ {
    case operations => CompositeToken(operations)
  }
  /* See other entries for examples.  An Expression is the fundamental evaluation unit of Muster. */
  lazy val EXPRESSION: Parser[Expression] = ( stringLit ^^ { case s => LiteralToken(s) }
                                            | numericLit ^^ { case s => LiteralToken(s) }
                                            | BLOCK
                                            | CONDITION
                                            | DEFINITION
                                            | DECLARATION
                                            | COMPOSITE
                                            | SERIAL
                                            | IDENTIFIER )
  /* Call semantics, the evaluation of the Operation on the right is supplied to the expression on the left
   * as input.
   */
  lazy val OPERATION: Parser[OperationToken] = EXPRESSION ~ ("<<" ~> OPERATION).? ^^ {
    case expression ~ operation => OperationToken(expression, operation)
  }
  /* Basic collection of operations.  Includes a Composite that defines inputs and names and a Program that
   * contains the series of Operations to evaluate.
   */
  lazy val BLOCK = COMPOSITE ~ ("{" ~> PROGRAM <~ "}") ^^ {
    case composite ~ program => BlockToken(composite, program)
  }
  /* Conditional Block.  The contents of the Block's Program are evaluated if the Composite of the Block
   * evaluates to a non-false value.
   */
  lazy val CONDITION = "when" ~> BLOCK ^^ { case condition =>  ConditionToken(condition) }
  /* Creates a block that takes 0 or more inputs.  The Block is not evaluated until call semantics are used. */
  lazy val DECLARATION = "declare" ~> BLOCK ^^ { case block => DeclarationToken(block) }
  /* Creates a label that can be used to reference a block. */
  lazy val DEFINITION = ("define" ~> IDENTIFIER) ~ (":=" ~> DECLARATION) ^^ {
    case identifier ~ declaration => DefinitionToken(identifier, declaration)
  }
  /* Signals that the contained Operation should be executed serially and not in parallel. */
  lazy val SERIAL = "serial" ~ "{" ~> OPERATION <~ "}" ^^ { case operation =>  SerialToken(operation) }
  /* A Program level Operation, delimited by ; */
  lazy val PROGRAM_OPERATION = OPERATION ~ ";" ^^ { case operation ~ _ => ProgramOperationToken(operation) }
  /* A Proram, or series of Operations to evaluate. */
  lazy val PROGRAM = (PROGRAM_OPERATION+) ^^ { case operations => ProgramToken(operations) }

  def main(args: Array[String]) {
    val input = Source.fromFile(args.head).getLines.reduceLeft[String](_ + '\n' + _)
    val tokens = new lexical.Scanner(input)
    val result = phrase(PROGRAM)(tokens)

    result match {
      case Success(tree, _) => new Interpreter(tree).run(args.toList.tail)
      case e: NoSuccess => {
        Console.err.println(e)
        sys.exit(100)
      }
    }
  }
}


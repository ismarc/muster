package com.muster

import com.muster.tokens.Token

case class Symbol (tree: Token, symbols: Map[String, Symbol])

package com.muster

class SymbolTable(symbols: Map[String, Symbol]) {
  def +(name: String, symbol: Symbol) = {
    val newSymbols = symbols + (name -> symbol)
    new SymbolTable(newSymbols)
  }

  def resolve(id: String): Option[Symbol] = {
    if (symbols.contains(id)) {
      Some(symbols(id))
    } else {
      None
    }
  }

  def resolve(ids: List[String]): Option[Symbol] = {
    if (ids.length == 1) {
      resolve(ids.head)
    } else {
      resolve(ids.head) match {
        case Some(symbol) => {
          (new SymbolTable(symbol.symbols)).resolve(ids.tail)
        }
        case None => {
          None
        }
      }
    }
  }
}

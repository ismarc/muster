package com.muster.tokens

case class BlockToken(composite: CompositeToken, statements: ProgramToken) extends Expression


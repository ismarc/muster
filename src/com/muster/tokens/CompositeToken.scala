package com.muster.tokens

case class CompositeToken(operations: Option[List[OperationToken]]) extends Expression

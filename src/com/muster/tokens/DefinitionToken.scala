package com.muster.tokens

case class DefinitionToken(identifier: IdentifierToken, declaration: DeclarationToken) extends Expression

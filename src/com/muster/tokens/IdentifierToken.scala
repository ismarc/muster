package com.muster.tokens

case class IdentifierToken(identifier: RawIdentifierToken, ident_type: Option[RawIdentifierToken]) extends Expression

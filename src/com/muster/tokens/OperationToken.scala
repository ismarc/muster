package com.muster.tokens

case class OperationToken(left: Expression, right: Option[OperationToken]) extends Token

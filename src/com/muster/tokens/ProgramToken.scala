package com.muster.tokens

case class ProgramToken(operations: List[ProgramOperationToken]) extends Token

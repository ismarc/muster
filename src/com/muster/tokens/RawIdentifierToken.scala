package com.muster.tokens

case class RawIdentifierToken(identifiers: List[String]) extends Token
